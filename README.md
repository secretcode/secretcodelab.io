[Secret Code](https://secretcode.gitlab.io/) is the official site of the Secret Code (jazz) band. 

See [about](https://secretcode.gitlab.io/about). 

Mail, pgp, links etc. of the incumbent maintainer: [about/ag](https://secretcode.gitlab.io/ag)
A reserve profile: [gravatar.com/aigodin](https://gravatar.com/aigodin). 

License: [Open Universe GPL](https://secretcode.gitlab.io/ou). 
