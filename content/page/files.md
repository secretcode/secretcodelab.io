﻿---
title: Files
comments: false
date: 2018-06-09T20:45:07+03:00
url: /files/
---

{{< directoryindex path="/static/files" pathURL="/files" >}} 
 

___
 

#### Texts
* [Pages](/page/) 
* [Posts](/map/) 
* [Qualia](https://alekseygodin.wordpress.com/qualia/) ` (wordpress) `

#### [GitLab](https://gitlab.com/secretcode/secretcode.gitlab.io) {#commit}
You may fork this project and commit to it 
(via simple & intuitively understandable way of Git: [1](https://stackoverflow.com/questions/32125514/how-to-create-and-commit-a-branch-in-gitlab), [2](https://stackoverflow.com/a/32127674), [3](https://akrabat.com/the-beginners-guide-to-contributing-to-a-github-project/), [4](https://git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project#_public_project), [5](https://help.github.com/articles/fork-a-repo), [6](https://stackoverflow.com/a/14680805), [7](https://docs.gitlab.com/ee/user/project/merge_requests/index.html), [8](https://guides.github.com/activities/hello-world/)). 

##### Fork
~~~
$ git clone https://gitlab.com/secretcode/secretcode.gitlab.io.git
~~~

 

#### See also 
* [/files/knjka](/files/knjka/) 
* [/files/knjka/source](/files/knjka/source/)
* [/files/archivvm](/files/archivvm/) 

 
