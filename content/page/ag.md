﻿---
title: aleksey godin
comments: false
date: 2018-12-01T13:43:09+03:00
url: /ag/
aliases: 
    - /about/ag/
---
<img alt="a.g."
src="/img/ag.jpg"
align="right" border="10" hspace="10" vspace="10" > 

 
The incumbent maintainer of the [` secret code `](/) project. 
(Several words [about](/about) it.)

My YoB: 1974. 

I was a translator, an editor, and so on. And also a journalist, writer, copywriter, manager, loader... . 

My primary function in our project has no relation to the process of a-working-model-of-the-world creation; rather, my job encompasses maintenance, that is technical, physical && mechanical support of it. 


### Email && pgp/gpg && π 
` aigodin @ yahoo.com ` 
[PGP/GPG](/pgp/): ` 24F1ACCEE27A178F ` 
[π](/pi/)


### Reserve profiles etc. 
* [gravatar.com/aigodin](https://en.gravatar.com/aigodin) 
* && 2d: [page.is/godin](https://page.is/godin) (& a contact form) (&& [a no-contact form](https://alekseygodin.wordpress.com/about/noncontact/))
* [qualia](https://alekseygodin.wordpress.com/qualia) (a Wordpress-based studio) 
* [inoreader.com/u/aleksey012](http://www.inoreader.com/u/aleksey012) (shared / broadcasted items via inoreader)
* [listed.to/@ag](https://listed.to/@ag) (standard notes) 
* [twitter.com/alekseygodin](https://twitter.com/alekseygodin) (my secret twitter #6) 
* [archive.org](https://archive.org/details/@a_i_godin?tab=web-archive)
	* [prehistory: замечание о старых текстах](/styopa/)
* [updates: rss etc.](/rss/)

 
