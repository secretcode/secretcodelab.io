﻿---
title: Секретная книжка
comments: false
date: 2018-12-18T13:18:27+03:00
url: /knjka/
aliases: 
    - /sk/
    - /sb/
---
<br><img src="/img/face.jpg" alt="" width="209" height="300"> 
 

<!---->_stable_
` 2.4.2 `: [pdf](/files/knjka/knjka.pdf)  [odt](/files/knjka/knjka.odt)  [[fonts]](/files/fonts.7z)
[read in gdocs viewer](https://docs.google.com/viewer?url=https://secretcode.gitlab.io/files/knjka/knjka.pdf)

_assembly-line_ 
– [epubs & docs](/files/knjka/epubs/) _(testing)_   ` 2.4.3b `
– [source](/files/knjka/source/) _(unstable)_ 

_updates_
[html](/categories/knjka/)  [rss](/categories/knjka/index.xml)  [via email](https://feedburner.google.com/fb/a/mailverify?uri=knjka&amp;loc=en_US)

[about](/knjka/about)

ⓞ лицензия: [open universe gpl](/openuniverse)
[my π](/pi)

image credit: [eso](http://www.eso.org/public/images/potw1644a/)
  
___

#### [txt]  {#txt}

  i. [Дрожь паутинки на ветру](/knjka/gossamer)   ` 2.4.3b `
 ii. [Tabula rasa](/knjka/tabula) 
iii. [Anno Domini MCMLXXXV](/knjka/anno)   <!--` 2.4.3b `-->
 iv. Arbor mundi
     [...]

 
___

 

