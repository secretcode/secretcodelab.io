﻿---
title: Конструкция “книжка” 
date: 2019-10-21T16:51:14+03:00 
type: knjka_about
url: /knjka/about/knjka/
aliases: 
    - /konstr0/
    - /constr0/
---
#### Модель модели мира 
* (Если ее нет, она [всё равно есть](https://secretcode.gitlab.io/plato_art/), но произвольная.) 
* (3 фабулы = OS модели мира.) 
* (Работающая модель мира — метафора, которая становится миром, потому что, возможно впервые, его конструирует.) 
* (Работающая модель реальности, которая становится реальностью.) 

#### Решение: функция функций 
* ` подлинный мир ` ↔ ` секретная книжка ` ↔ ` “секретная книжка” `
    ` “секретная книжка” ` ↔ ` 3 фабулы ` ↔ ` конструкции `
	Общая функция реальности ([Ψ](https://en.wikipedia.org/w/index.php?title=Wave_function&printable=yes)), ↔ которую мы пытаемся понять, посредством конструкций, ↔ то, что мы из этого понимания создаем. 
* Подлинный мир = среда обитания. 
	* Но в действительности мы персонажи секретной книжки :). Мы можем видеть только ее. 
* В подлинном мире существуют _все_ сюжеты. 
	* Одни сюжеты возможны, другие невозможны. 
		* Подлинный мир = логическая структура мира. 
	* Реальность секретной книжки — пересечение того, что <i>было</i> (например, столкновение Одиссея с циклопом). Всё могло (бы) быть, но было только _это_. 
* Подлинный мир никогда и никем не может быть о/написан (иначе он перестал бы быть подлинным миром — он неописуем). 
	* Секретная книжка = мир ее персонажей. 

#### Самая главная сказка 
* Сказка под названием “секретная книжка” (которую пишет группа secret code) не является секретной книжкой. 
	* Однако это сказка _про_ секретную книжку, и потому она (и, по большому счету, только она) имеет значение. 
* Сказка только речь, и условность. Реальны конструкции и их пересечения. Конструкции растут в подлинном мире и должны быть отчасти реальны :), по крайней мере это верно для их пересечений. Сказка — их репрезентация. Сказка — ничто, но позволяет увидеть что. 
	* Секретная книжка, как та, так и эта, — только метафоры. Волшебные зеркала, в которых видна вся реальность. Самостоятельного значения не имеют. 
* Конструкции связаны чем-то единым. Назовем это функцией. Если она работает, то это логическая структура мира as is. Ψ. Подлинный мир. 
	* (Предмет [фабулы 1](https://secretcode.gitlab.io/knjka/about/fabula1/).) 
	* Ψ — тоже метафора. (Основная реальность основной метафоры — то есть большой секретной книжки.) 
* Сказка, та или иная, в той или иной мере удачно, переводит функцию в речь. С самого начала ясно, что ничего не ясно и всё условно. Но метод позволяет, хотя бы отчасти, приблизиться к реальности. 
* Основные конструкции сказки:  
	* Книжка [модель модели]. Условность условности: рамка. Рамка пуста. Конструкция 0. Конструкция “книжка”. 
	* [Фабула 1](https://secretcode.gitlab.io/knjka/about/fabula1/). Модель общей системы. То, что видно из фабулы 2, с границ реальности. 
	* Фабула 2 [история, место человека в мире, собственно реальность, которую мы создаем: модель мира в действии]. История секретной книжки. Место человечков в этой истории. 
	* История делается фабулой 3. Плоть и кровь этой реальности. 
		* Конструкции фабулы 3 могут выглядеть по-разному, в зависимости от физических характеристик пространства, в котором они нарастают. Фабула 3 — текст и значения — постоянно меняется. 
		* Потому что меняется не только реальность, но и наше понимание реальности. 
		* (Фабула 3, по большому счету, --- только пересечение многих частных фабул 2.)
	* Если фабула 2 неизменна (это движение as is), фабула 1 и сама книжка — модель и функция фабул — неизменна в принципе. 
		* Иными словами, модель статична, и у нее есть принципы. Однако они не ригидны, любое выражение динамично. Всё постоянно меняется. 
		* Модель жизни похожа на жизнь. :) 
			* Для персонажей существует только секретная книжка. Всё прочее — ее экстраполяции и отражения. 
			* (С точки зрения персонажей.) 
* Нельзя сказать: фабула 2 растет из фабулы 3, а из нее растет фабула 1. Они работают только вместе, сами по себе они ничто. 
    * На самом-то деле всё просто. 
        * Фабула 3 — основные конструкции про всё подряд — совокупность инсайтов обо всем подряд. 
        * Инсайты должен кто-то иметь. Агенты, которые в истории и мире. Фабула 2 — агенты в истории и мире. 
        * Фабула 1 — то, что они, в минуты просветления, видят в общей системе, насколько они ее могут понять. 
			* Однако история (фабула 2) не может выйти за рамки общей системы (фабулы 1), а всё, что с нами происходит, неизбежно остается в рамках истории. 
			* В конечном счете это динамическая и живая система: потому что та или иная формулировка одной фабулы влияет на другие. 

#### Модель моделей и принцип ее действия
* Просветление каждого атома общей системы. 
* Возможность взгляда вовне: с точки зрения мира как единого целого. 
* Всё идет к тому, что рано или поздно информационные системы (культуры) секретной книжки объединятся и будут включать в себя всё — не только всю возможную информацию (включенную в общую сферу смысла), но и физ. процессы. 
* Подлинный мир станет секретной книжкой. 
* Секретная книжка станет подлинным миром. 
* Само собой, создать такую “секретную книжку”, которая была бы подлинным миром, невозможно в принципе. Не говоря о том, что это невозможно здесь и сейчас. 
* Но можно приблизительно представить ее модель. 
* Даже если бы она получилась только на 1%, это значило бы, что в ней нет ничего личного. Свойственного тому или иному отдельному человеку. 
* В общей системе все (со)участвуют невольно. Книжка — только модель, в которой система (само)сознается. И, таким образом, меняется или может измениться. Волшебное зеркало. 
    * Без зеркала, в любом случае, не видно ничего. 

 

