﻿---
title: Структура структуры
date: 2019-10-23T06:20:59+03:00
type: knjka_about
url: /knjka/about/structure/
aliases: 
    - /knjka/structure/
    - /str_str/
---

* [Конструкция “книжка”](/knjka/about/knjka/). Модель моделей. 
	* [3 фабулы](/knjka/about/fabula123/). 
	    * [Фабула 1](/knjka/about/fabula1/). Эскиз общей системы. 
	        * [Ψ](/knjka/about/fabula1/psi/). О реальности сновидений. 
	    * Фабула 2. Путь как процесс (создания книжки / мира). 

` A note `. Все эти конструкции устарели, да и, по большому счету, изначально не были важны. 

 

