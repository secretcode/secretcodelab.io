﻿---
title: files/knjka
comments: false
date: 2018-09-21T18:17:04+03:00
url: /files/knjka/
aliases: 
    - /stable/
---

{{< directoryindex path="/static/files/knjka" pathURL="/files/knjka" >}} 
 

___
 

Это текущая стабильная версия (номер указан в конце). 
Прежние см. в [` archivvm `](/files/archivvm/). 
 
((` .pdf ` — как правило ` hybrid PDF ` (то есть ` ODF embedded `) (можно открыть в LibreOffice как обычный ` .odt `).)) 
 

#### See also 
* [/files](/files/)
* [/files/archivvm](/files/archivvm/) 

 
