﻿---
title: about
comments: false
date: 2018-06-30T21:47:09+03:00
url: /about/
---
Secret Code is a (rock) group. 

Currently we play only one album: [The Secret Book](/knjka/). 
The incumbent maintainer of the project is [aleksey godin](/ag/). 
[Qualia](https://alekseygodin.wordpress.com/qualia) is a Wordpress-based studio of Secret Code. 

Время от времени группа secret code репрезентируется (se représente) под незарегистрированным товарным знаком "Андрей Чевакинский". 

### License 
Все оригинальные произведения группы Secret Code распространяются по лицензии [Open Secret GPL](/opensecret/). 
Любой желающий может делать с ними что угодно 
(, но please consider обратить внимание на [π](/pi/)). 

### [Files](/files/)
&& texts: all of them

### [How to subscribe](/rss/)
(RSS, email subscription etc.) 

### [π](/pi/)
Реквизиты & wish list.

### Our permanent headquarters in Gabon
[secretcode.ga](http://secretcode.ga) 
((That is, a quick address. :)) 

 
