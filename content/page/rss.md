﻿---
title: RSS etc.
comments: false
date: 2018-06-30T16:02:02+03:00
url: /rss/
aliases: 
    - /rss.rdf
    - /subscribe/
    - /subscriptions/
    - /subscription/
    - /feed/
    - /rss.xml
---
### [a] [secretcode.gitlab.io](/) 
1. __[Categories: posts](/categories/posts/)__, or _something important_: [rss](/categories/posts/index.xml), or [get updates via email](https://feedburner.google.com/fb/a/mailverify?uri=aleksey_godin&amp;loc=en_US).
1. [Всё, что про книжку](/categories/knjka/): [rss](/categories/knjka/index.xml), или [via email](https://feedburner.google.com/fb/a/mailverify?uri=knjka&amp;loc=en_US).
1. _[Categories: qualia](/categories/qualia/)_, or _twitter #11_: [rss](/categories/qualia/index.xml). 
1. [Post](/post/)(s): общий стрим сообщений: [rss](/post/index.xml). 
    * [Pages](/page/): общий стрим новых страниц: [rss](/page/index.xml). 
        * Общий стрим всего минус git: [rss](/index.xml).
1. [Новости проекта](https://gitlab.com/secretcode/secretcode.gitlab.io/): всё, чего нового увидел git: [rss](https://gitlab.com/secretcode/secretcode.gitlab.io.atom).
    * [commits](https://gitlab.com/secretcode/secretcode.gitlab.io/commits/): [rss](https://gitlab.com/secretcode/secretcode.gitlab.io/commits/master?format=atom). 

### [b] qualia 
* [Qualia](https://alekseygodin.wordpress.com/qualia/): gitlab's posts [1, 1] + qualia (_per se_) — preprints & stream of consciousness: 
[rss](https://alekseygodin.wordpress.com/feed/), or [email](https://alekseygodin.wordpress.com/about/subscribe/subscribe_via_email/). 

### [c] all of it
* [Rss](https://feeds.feedburner.com/all_of_it/), or [email](https://feedburner.google.com/fb/a/mailverify?uri=all_of_it). 

 

