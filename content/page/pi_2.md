﻿---
title: Реквизиты π 
date: 2019-06-29T10:30:26+03:00
url: /pi/2/
aliases: 
    - /pi2/
    - /pi_2/
---
#### PayPal 
[Прямая ссылка на форму](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=MWXAX4RW2KM3U&source=url). 

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick" />
<input type="hidden" name="hosted_button_id" value="MWXAX4RW2KM3U" />
<input type="image" src="https://www.paypalobjects.com/en_US/RU/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
<img alt="" border="0" src="https://www.paypal.com/en_RU/i/scr/pixel.gif" width="1" height="1" />
</form>

 
Также что-то вроде профиля в этой системе: 
[paypal.me/aigodin](https://paypal.me/aigodin). 

Код несекретный (ссылка на этот адрес): 

![](/img/paypal.png) 
 

#### Сбербанк 
Номер карты ` Visa ` — 

<pre>4817 7602 5721 3086</pre>

Через мобильный банк Сбербанка — SMS на номер 900: 

<pre>perevod 4817760257213086 314</pre>

(для перевода 314 р. — система примет и другие цифры :) 
(наверное, оптимальное (для всех) понимание _π_ в этом контексте — [3.14 USD in RUR](https://duckduckgo.com/?q=3.14+USD+in+RUR&ia=currency)). 
 

#### Master card
Через систему, которая принимает переводы для ` Master card ` (банкоматы и т. д.), можно перевести ` 3,14 ` на карту 

<pre>5391 7500 2736 9921</pre>

Лучше еще и сообщить о факте через [бесконтактную форму](https://alekseygodin.wordpress.com/about/noncontact/). 
 

#### Скинуть на номер телефона
Для жителей России, наверное, проще всего скинуть на номер телефона. Любым удобным способом: 

<pre>+7 925 078 99 21</pre>

Это, btw, МегаФон, но с номера на номер могут пересылать абоненты и БиЛайна, и МТС. 

Инструкции для [МегаФона](https://moscow.megafon.ru/services/finance/mobilnyy_perevod/mobilnyy_perevod.html), [БиЛайна](https://moskva.beeline.ru/customers/pomosh/pay/sposoby-oplaty/oplata-s-telefona-na-telefon/), [МТС](https://moskva.mts.ru/personal/dengi/denezhnie-perevodi/perevod-po-sms), [Tele2](https://msk.tele2.ru/option/mobile-transfer).
 

#### Bitcoin address

<pre>1Ns5kqHxcxt7gJvVQNKvEWcH7Yrf7r1qpQ</pre>

![](/img/bitcoin.png)

Это для небольших операций. Для больших и серьезных (Open Universe — это серьезно :) есть cold storage и одноразовые wallets.  Адрес сообщается по запросу. Только обязательно нужно пользовать [pgp](/pgp/), а то иначе не по-взрослому. 
 

#### Банковские переводы 
К сожалению, PayPal (особенно российский), Master card и МегаФон — службы с ограничениями. Не уверен, что $ 20 млн через них пролезут. На этот случай — конвенциональный банковский перевод. 

Наверное, это возможно через банкомат, но в любом случае — через людей в банке, которые в окне: 

<pre>SWIFT-КОД Сбербанка:
SABRRUMM

Мой р/с:
4081 781 053 81 705 730 41

GODIN ALEXEY IGOREVICH

Банк получателя: 
SBERBANK (HEAD OFFICE — ALL BRANCHES AND OFFICES 
IN RUSSIA) MOSCOW RUSSIAN FEDERATION

Код подразделения: 
3890 38016 96
</pre>

Это работает, если платить не из РФ. 

Из РФ работает обычный банковский перевод:

<pre>ГОДИН АЛЕКСЕЙ ИГОРЕВИЧ

Мой р/с:
4081 781 053 81 705 730 41

ПАО СБЕРБАНК Г. МОСКВА 
№ 9038/01696

ИНН БАНКА 7707083893
БИК БАНКА 044525225 </pre>

 
### Запрет деанона
Согласно лицензии [Open Universe GPL](/openuniverse/), плата за открытые секреты _должна_ быть анонимной. Анонимность пока что не реализована физически. Это будет осуществлено, когда у Open Universe GPL появится собственная (dedicated) платежная система. 

(Пока что получатели должны игнорировать поле ` from `). 

Платить за открытые секреты не имеют права те, кто участвует в распространении секрета и, добавлю от себя, те, кто знаком с авторами. В том числе состоял в переписке, в кофолловерах и т. д. 

 
