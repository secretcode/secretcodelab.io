---
title: secretum
comments: false
date: 2018-06-05T16:25:25+03:00
url: /secretum/
aliases: 
    - /secretvm/
    - /secret/
---
<iframe src="https://app.box.com/embed/s/nttxnmnnds08zejdgg9h?showParentPath=false&sortColumn=date&view=list" width="500" height="700" frameborder="0" allowfullscreen webkitallowfullscreen msallowfullscreen></iframe> 

Physically this folder is here: [box.com/secretum](https://app.box.com/v/secretum). 

Я иногда сбрасываю туда осколки, которые жалко выбрасывать. 
Туда же уходят (иногда) предварительные сборки. 
 

<!--### О последовательности версий {#sequence}
В [source](/files/knjka/source/) — самые последние версии; 
– [epubs etc.](/secretum/knjka/epubs/) = [текстам на сайте](/knjka/#txt); 
– [stable](/files/knjka/) —  то, что не меняется давно. 

В секрете, однако, иногда сохраняются самые последние и самые нестабильные версии. 
 

### See also
* archive.org: 
    * [page](https://web.archive.org/web/*/secretcode.gitlab.io/)
    * [pages & files](https://web.archive.org/web/*/secretcode.gitlab.io/*)
* [secretum/knjka](/secretum/knjka/)
    * [prehistory](/secretum/2/)-->