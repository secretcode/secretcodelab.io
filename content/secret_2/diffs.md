﻿---
title: Diffs for *.odt (etc.) files
date: 2019-04-09T21:06:47+03:00
url: /files/knjka/source/2/
aliases: 
    - /odt/
    - /diffs/
---
Use [this trick](https://medium.com/@mbrehin/git-advanced-diff-odt-pdf-doc-xls-ppt-25afbf4f1105): 

1. Install ` odt2txt ` (Linux: ` sudo apt-get install odt2txt `, Mac: ` brew install odt2txt `). 
2. Add to ` .gitattributes `: 

    ~~~~
    *.odt diff=odt 
    ~~~~ 
    
3. And to ` .gitconfig `: 

    ~~~~
    [diff "odt"]
        textconv = odt2txt
        binary = true
    ~~~~ 
    
4. Of course it will work better if you have cloned this project :), and are fetching updates, and run commands like ` log `. (But you should see the diffs even in GUI tools like ` gitk `.) 

    To see the ` log ` of 20 last commits I use 
    
    ~~~~
    $ git --no-pager log --max-count=20 -p --word-diff=color
    ~~~~ 
    
    And for ` diff `: 
    
    ~~~~
    $ git --no-pager diff --word-diff=color 
    ~~~~ 
    
    Probably, there should be another trick, to keep the words unsplit, but I haven't figured it out yet. 

 
